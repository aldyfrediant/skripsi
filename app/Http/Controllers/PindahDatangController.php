<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\ActionLog;
use App\Pindah;
use DB;
use DataTables;

class PindahDatangController extends Controller
{
    private $logAction;

    public function __construct()
    {
      $this->logAction = New ActionLog;
      $this->logAction->id_user = 1; //should be changed
      $this->logAction->group = "Pindah Datang";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = 'datang';

        return view('admin.data_pindah.list')
            ->with('type', $type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = New Pindah;
        $title = "Tambah Data Pindah Datang";
        $action = "datang.store";
        $type = 'datang';

        return view('admin.data_pindah.form')
            ->with('data', $data)
            ->with('title', $title)
            ->with('action', $action)
            ->with('type', $type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Pindah;        

        $this->validate($request, [
            'noreg' => 'required',
            'nik' => 'required',
            'noKK' => 'required',
            'nama' => 'required',
            'kotaAsal' => 'required',
            'kecAsal' => 'required',
            'alamatLama' => 'required',
            'alamatBaru' => 'required',
            'kelurahan' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
            'jk' => 'required',
        ]);        
        
        $data->no_registrasi = $request->noreg;
        $data->nik = $request->nik;
        $data->no_kk = $request->noKK;
        $data->nama = $request->nama;
        $data->kota_asal = $request->kotaAsal;
        $data->kecamatan_asal = $request->kecAsal;
        $data->alamat_lama = $request->alamatLama;
        $data->alamat_baru = $request->alamatBaru;
        $data->kelurahan = $request->kelurahan;
        $data->agama = $request->agama;
        $data->jenis_kelamin = $request->jk;
        $data->kewarganegaraan = $request->kewarganegaraan;
        $data->status = 1;

        $data->kota_tujuan = "Bandung";
        $data->kecamatan_tujuan = "Regol";
        $data->jenis = "datang";

        $this->logAction->action = "Tambah Data Pindah Datang- ".$request->noreg;
        
        try{
            $data->save();
            $this->logAction->item_id = $data->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('datang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pindah::find($id);
        $title = "Edit Data Pindah Datang";
        $action = "datang.update";
        $type = "datang";

        return view('admin.data_pindah.form')
            ->with('data', $data)
            ->with('type', $type)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Pindah::find($id);        

        $this->validate($request, [
            'noreg' => 'required',
            'nik' => 'required',
            'noKK' => 'required',
            'nama' => 'required',
            'kotaAsal' => 'required',
            'kecAsal' => 'required',
            'alamatLama' => 'required',
            'alamatBaru' => 'required',
            'kelurahan' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
            'jk' => 'required',
        ]);        
        
        $data->no_registrasi = $request->noreg;
        $data->nik = $request->nik;
        $data->no_kk = $request->noKK;
        $data->nama = $request->nama;
        $data->kota_asal = $request->kotaAsal;
        $data->kecamatan_asal = $request->kecAsal;
        $data->alamat_lama = $request->alamatLama;
        $data->alamat_baru = $request->alamatBaru;
        $data->kelurahan = $request->kelurahan;
        $data->agama = $request->agama;
        $data->jenis_kelamin = $request->jk;
        $data->kewarganegaraan = $request->kewarganegaraan;

        $data->kota_tujuan = "Bandung";
        $data->kecamatan_tujuan = "Regol";

        $this->logAction->action = "Ubah Data Pindah Datang- ".$request->noreg;
        
        try{
            $data->save();
            $this->logAction->item_id = $data->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('datang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pindah::find($id);

        $this->logAction->action = "Hapus Data Pindah Datang - ".$data->no_registrasi;

        try{
            $this->logAction->item_id = $data->id; 
            Pindah::destroy($id);
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('datang.index');
    }

    /**
     * Retrieve Pindah by query via ajax for datatables 
     *
     * @param   datatable default JSON output
     * @return  datatable JSON input
        int draw //page
        int recordsTotal //total records in database
        int recordsFiltered //
        string error (optional) //error message
     */

    public function print(Request $request){        
      $q = DB::table('pindahs')->select(['id', 'no_registrasi', 'nik', 'no_kk', 'nama', 'kota_asal', 'kecamatan_asal', 'alamat_lama', 'alamat_baru', 'jenis', 'kelurahan', 'jenis_kelamin'])
        ->where('jenis', '=', 'datang');

        if($request->has('nama'))
        {
            $q->where('nama', 'like', "%{$request->nama}%");
        }
        if($request->has('nik'))
        {
            $q->where('nik', 'like', "%{$request->nik}%");
        }
        if($request->has('noreg'))
        {
            $q->where('no_registrasi', 'like', "%{$request->noreg}%");
        }
        if($request->has('nokk'))
        {
            $q->where('no_kk', 'like', "%{$request->nokk}%");
        }
        if($request->has('tglAwal'))
        {
            $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
            $q->where('created_at', '>', $start);
        }
        if($request->has('tglAkhir'))
        {
            $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
            $q->where('created_at', '<', $end);
        }
        if($request->jk != "Semua")
        {
            $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
        }
        if($request->warga != "Semua")
        {
            $q->where('kewarganegaraan', 'like', "%{$request->warga}%");
        }
        if($request->agama != "Semua")
        {
            $q->where('agama', 'like', "%{$request->agama}%");
        }
        if($request->kelurahan != "Semua")
        {
            $q->where('kelurahan', 'like', "%{$request->kelurahan}%");
        }
        if($request->has('kotaAsal'))
        {
            $q->where('kota_asal', 'like', "%{$request->kotaAsal}%");
        }
        if($request->has('kecAsal'))
        {
            $q->where('kecamatan_asal', 'like', "%{$request->kecAsal}%");
        }
        if($request->has('alamatLama'))
        {
            $q->where('alamat_lama', 'like', "%{$request->alamatLama}%");
        }
        if($request->has('alamatBaru'))
        {
            $q->where('alamat_baru', 'like', "%{$request->alamatBaru}%");
        }
        if($request->has('status') && $request->status != "Semua")
        {
            $q->where('status', '=', $request->status);
        }

        $data = $q->get();
        $count = $q->count();

        return view('admin.data_pindah.print')
            ->with('q', $data)
            ->with('filter', $request)
            ->with('count', $count)
            ->with('type', 'datang')
            ->with('typeLaporan', "Data Pindah Datang");
    }

        
    public function getDatatableDatangs(Request $request){
      $data = DB::table('pindahs')->select(['id', 'no_registrasi', 'nik', 'no_kk', 'nama', 'kota_asal', 'kecamatan_asal', 'alamat_lama', 'alamat_baru', 'jenis', 'kelurahan', 'jenis_kelamin', 'status'])
        ->where('jenis', '=', 'datang');

      return DataTables::of($data)
      ->filter(function($q) use ($request){
            if($request->has('nama'))
            {
                $q->where('nama', 'like', "%{$request->nama}%");
            }
            if($request->has('nik'))
            {
                $q->where('nik', 'like', "%{$request->nik}%");
            }
            if($request->has('noreg'))
            {
                $q->where('no_registrasi', 'like', "%{$request->noreg}%");
            }
            if($request->has('nokk'))
            {
                $q->where('no_kk', 'like', "%{$request->nokk}%");
            }
            if($request->has('tglAwal'))
            {
                $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
                $q->where('created_at', '>', $start);
            }
            if($request->has('tglAkhir'))
            {
                $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
                $q->where('created_at', '<', $end);
            }
            if($request->jk != "Semua")
            {
                $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
            }
            if($request->warga != "Semua")
            {
                $q->where('kewarganegaraan', 'like', "%{$request->warga}%");
            }
            if($request->agama != "Semua")
            {
                $q->where('agama', 'like', "%{$request->agama}%");
            }
            if($request->kelurahan != "Semua")
            {
                $q->where('kelurahan', 'like', "%{$request->kelurahan}%");
            }
            if($request->has('kotaAsal'))
            {
                $q->where('kota_asal', 'like', "%{$request->kotaAsal}%");
            }
            if($request->has('kecAsal'))
            {
                $q->where('kecamatan_asal', 'like', "%{$request->kecAsal}%");
            }
            if($request->has('alamatLama'))
            {
                $q->where('alamat_lama', 'like', "%{$request->alamatLama}%");
            }
            if($request->has('alamatBaru'))
            {
                $q->where('alamat_baru', 'like', "%{$request->alamatBaru}%");
            }
            if($request->has('status') && $request->status != "Semua")
            {
                $q->where('status', '=', $request->status);
            }
        })
        ->addColumn('action', function($d){
            if($d->status == 0){
                $s0 = "<a href='".url("admin/pindah/datang")."/".$d->id."/pending_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Tunda' disabled><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/pindah/datang")."/".$d->id."/process_document' class='btn btn-xs btn-icon btn-warning' data-toggle='tooltip' title='' data-original-title='Proses'><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/pindah/datang")."/".$d->id."/finish_document' class='btn btn-xs btn-icon btn-success' data-toggle='tooltip' title='' data-original-title='Selesai'><i class='fa fa-check'></i></a>";
            }
            if($d->status == 1){
                $s0 = "<a href='".url("admin/pindah/datang")."/".$d->id."/pending_document' class='btn btn-xs btn-icon btn-danger' data-toggle='tooltip' title='' data-original-title='Tunda'><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/pindah/datang")."/".$d->id."/process_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Proses' disabled><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/pindah/datang")."/".$d->id."/finish_document' class='btn btn-xs btn-icon btn-success' data-toggle='tooltip' title='' data-original-title='Selesai'><i class='fa fa-check'></i></a>";
            }
            if($d->status == 2){
                $s0 = "<a href='".url("admin/pindah/datang")."/".$d->id."/pending_document' class='btn btn-xs btn-icon btn-danger' data-toggle='tooltip' title='' data-original-title='Tunda'><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/pindah/datang")."/".$d->id."/process_document' class='btn btn-xs btn-icon btn-warning' data-toggle='tooltip' title='' data-original-title='Proses'><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/pindah/datang")."/".$d->id."/finish_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Selesai' disabled><i class='fa fa-check'></i></a>";
            }            
            return("$s0 $s1 $s2
                <a href='".url('admin/pindah/datang')."/".$d->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$d->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data' style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>");
        })
        ->addColumn('asal', function($d){
            return($d->kecamatan_asal.', '.$d->kota_asal);
        })
        ->editColumn('status', function($d){
            if($d->status == 0)
                return("<span class='label label-danger'>Ditunda</span>");
            if($d->status == 1)
                return("<span class='label label-warning'>Diproses</span>");
            if($d->status == 2)
                return("<span class='label label-success'>Selesai</span>");
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    }

    public function pendingDoc($id)
    {      
        $pindah = Pindah::find($id);
        $this->logAction->action = "Update Status Data Pindah Datang[Tunda] - ".$pindah->no_registrasi;

        try{
            $pindah->status = 0;
            $pindah->save();
            $this->logAction->item_id = $pindah->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('datang.index');
    }

    public function processDoc($id)
    {
        $pindah = Pindah::find($id);
        $this->logAction->action = "Update Status Data Pindah Datang [Proses] - ".$pindah->no_registrasi;

        try{
            $pindah->status = 1;
            $pindah->save();
            $this->logAction->item_id = $pindah->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('datang.index');
    }

    public function finishDoc($id)
    {
        $pindah = Pindah::find($id);
        $this->logAction->action = "Update Status Data Pindah Datang [Selesai] - ".$pindah->no_registrasi;

        try{
            $pindah->status = 2;
            $pindah->save();
            $this->logAction->item_id = $pindah->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('datang.index');
    }
}
