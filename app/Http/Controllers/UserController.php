<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\ActionLog;
use App\User;
use DB;
use DataTables;

class UserController extends Controller
{
    private $logAction;

    public function __construct()
    {
      $this->logAction = New ActionLog;
      $this->logAction->id_user = 1; //should be changed
      $this->logAction->group = "User Management";
    }
    public function index()
    {
        return view('admin.user_management.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = New User;
        $title = "Tambah Data User";
        $action = "user_management.store";

        return view('admin.user_management.form')
            ->with('user', $user)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;        

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required|confirmed',
            'role' => 'required',
        ]);        
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role = $request->role;  
        $user->password = Hash::make($request->password);

        $this->logAction->action = "Tambah Data User - ".$request->username;
        
        try{
            $user->save();
            $this->logAction->item_id = $user->id;
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('user_management.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $title = "Edit Data User";
        $action = "user_management.update";

        return view('admin.user_management.form')
            ->with('user', $user)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);        

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required|confirmed',
            'role' => 'required',
        ]);        
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role = $request->role;  
        $user->password = Hash::make($request->password);

        $this->logAction->action = "Ubah Data User - ".$request->username;
        
        try{
            $user->save();
            $this->logAction->item_id = $user->id;
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('user_management.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $this->logAction->action = "Hapus Data User - ".$user->username;

        try{
            $this->logAction->item_id = $user->id;
            User::destroy($id);
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('user_management.index');
    }
    /**
     * Retrieve offices by query via ajax for datatables 
     *
     * @param   datatable default JSON output
     * @return  datatable JSON input
        int draw //page
        int recordsTotal //total records in database
        int recordsFiltered //
        string error (optional) //error message
     */
    public function getDatatableUsers(Request $request){
      $users = DB::table('users')->select(['id', 'name', 'username', 'email', 'role']);

      return DataTables::of($users)
        ->filter(function($q) use ($request){            
            if($request->has('nama'))
            {
                $q->where('name', 'like', "%{$request->nama}%");
            }         
            if($request->has('username'))
            {
                $q->where('username', 'like', "%{$request->username}%");
            }         
            if($request->has('email'))
            {
                $q->where('email', 'like', "%{$request->email}%");
            }         
            if($request->role != "Semua")
            {
                $q->where('role', 'like', "%{$request->role}%");
            }
        })
        ->addColumn('action', function($u){
            return("
                <a href='".url('admin/user_management')."/".$u->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$u->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data' style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>");
        })
        ->make(true);
    }

    public function pass(){
      return view('admin.changepass');
    }

    public function updatePass(Request $request){
        $user = User::find(Auth::user()->id);        

        $this->validate($request, [
            'password' => 'required|confirmed',
        ]);

        $this->logAction->action = "Ganti Password - ".$user->username;
        $user->password = Hash::make($request->password);

        try{
            $this->logAction->item_id = $user->id;
            $user->save();
            $this->logAction->save(); 
        } 
        catch(\Exception $e)
        {}

        return redirect('admin'); 
    }
}
