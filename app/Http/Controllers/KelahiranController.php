<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\ActionLog;
use App\Kelahiran;
use App\KartuKeluarga;
use App\Penduduk;
use DB;
use DataTables;

class KelahiranController extends Controller
{
    private $logAction;

    public function __construct()
    {

      $this->logAction = New ActionLog;
      $this->logAction->id_user = 1; //should be changed
      $this->logAction->group = "Kelahiran"; //should be changed
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = 'normal';
        return view('admin.data_kelahiran.list')
            ->with('type', $type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelahiran = New Kelahiran;
        $title = "Tambah Data Kelahiran";
        $action = "kelahiran.store";

        return view('admin.data_kelahiran.form')
            ->with('kelahiran', $kelahiran)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kelahiran = new Kelahiran;        

        $this->validate($request, [
            'noSkk' => 'required',
            'nama' => 'required',
            'idKK' => 'required',
            'waktuLahir' => 'required',
            'tempatLahir' => 'required',
            'namaAyah' => 'required',
            'namaIbu' => 'required',
            'tempatNikah' => 'required',
            'tanggalNikah' => 'required',
            'noBukuNikah' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
        ]);        
        
        $kelahiran->no_skk = $request->noSkk;
        $kelahiran->nama = $request->nama;
        $kelahiran->id_kk = $request->idKK;
        $kelahiran->waktu_kelahiran =  date("Y-m-d h:m:s", strtotime($request->waktuLahir));
        $kelahiran->tempat_lahir = $request->tempatLahir;
        $kelahiran->agama = $request->agama;
        $kelahiran->jenis_kelamin = $request->jk;
        $kelahiran->kewarganegaraan = $request->kewarganegaraan;
        $kelahiran->nama_ayah = $request->namaAyah;
        $kelahiran->nama_ibu = $request->namaIbu;
        $kelahiran->tempat_nikah = $request->tempatNikah;
        $kelahiran->tanggal_nikah = date("Y-m-d", strtotime($request->tanggalNikah));
        $kelahiran->no_buku_nikah = $request->noBukuNikah;
        $kelahiran->status = 1;

        $this->logAction->action = "Tambah Data Kelahiran- ".$request->noSkk;
        
        try{
            $kelahiran->save();
            $this->logAction->item_id = $kelahiran->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('kelahiran.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelahiran = Kelahiran::find($id);
        $dataKK = KartuKeluarga::find($kelahiran->id_kk);
        $title = "Edit Data Kelahiran";
        $action = "kelahiran.update";

        return view('admin.data_kelahiran.form')
            ->with('kelahiran', $kelahiran)
            ->with('dataKK', $dataKK)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelahiran = Kelahiran::find($id);        

        $this->validate($request, [
            'noSkk' => 'required',
            'nama' => 'required',
            'idKK' => 'required',
            'waktuLahir' => 'required',
            'tempatLahir' => 'required',
            'namaAyah' => 'required',
            'namaIbu' => 'required',
            'tempatNikah' => 'required',
            'tanggalNikah' => 'required',
            'noBukuNikah' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
        ]);        
        
        $kelahiran->no_skk = $request->noSkk;
        $kelahiran->nama = $request->nama;
        $kelahiran->id_kk = $request->idKK;
        $kelahiran->waktu_kelahiran =  date("Y-m-d h:m:s", strtotime($request->waktuLahir));
        $kelahiran->tempat_lahir = $request->tempatLahir;
        $kelahiran->nama_ayah = $request->namaAyah;
        $kelahiran->nama_ibu = $request->namaIbu;
        $kelahiran->agama = $request->agama;
        $kelahiran->jenis_kelamin = $request->jk;
        $kelahiran->kewarganegaraan = $request->kewarganegaraan;
        $kelahiran->tempat_nikah = $request->tempatNikah;
        $kelahiran->tanggal_nikah = date("Y-m-d", strtotime($request->tanggalNikah));
        $kelahiran->no_buku_nikah = $request->noBukuNikah;

        $this->logAction->action = "Edit Data Kelahiran- ".$request->noSkk;
        
        try{
            $kelahiran->save();
            $this->logAction->item_id = $kelahiran->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('kelahiran.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelahiran = Kelahiran::find($id);

        $this->logAction->action = "Hapus Data Kelahiran - ".$kelahiran->no_skk;

        try{
            $this->logAction->item_id = $kelahiran->id; 
            Kelahiran::destroy($id);
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('kelahiran.index');
    }

    public function print(Request $request){
        $q = DB::table('kelahirans')->select(['kelahirans.id', 'no_skk', 'nama', 'waktu_kelahiran', 'tempat_lahir', 'agama', 'jenis_kelamin', 'kewarganegaraan', 'nama_ibu', 'nama_ayah', 'tempat_nikah', 'tanggal_nikah', 'no_buku_nikah', 'kelahirans.created_at', 'id_kk', 'kartu_keluargas.kelurahan', 'alamat', 'rt', 'rw', 'status'])
            ->join('kartu_keluargas', 'kartu_keluargas.id', '=', 'kelahirans.id_kk');

        if($request->has('noSkk'))
        {
            $q->where('no_skk', 'like', "%{$request->noSkk}%");
        }
        if($request->has('nama'))
        {
            $q->where('nama', 'like', "%{$request->nama}%");
        }
        if($request->has('tglAwal'))
        {
            $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
            $q->where('kelahirans.created_at', '>', $start);
        }
        if($request->has('tglAkhir'))
        {
            $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
            $q->where('kelahirans.created_at', '<', $end);
        }
        if($request->has('tempatLahir'))
        {
            $q->where('tempat_lahir', 'like', "%{$request->tempatLahir}%");
        }
        if($request->has('lhrAwal'))
        {
            $lStart = date("Y-m-d 00:00:00", strtotime($request->lhrAwal));
            $q->where('waktu_kelahiran', '>', $lStart);
        }
        if($request->has('lhrAkhir'))
        {
            $lEnd = date("Y-m-d 23:59:59", strtotime($request->lhrAkhir));
            $q->where('waktu_kelahiran', '<', $lEnd);
        }
        if($request->jk != "Semua")
        {
            $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
        }
        if($request->warga != "Semua")
        {
            $q->where('kewarganegaraan', 'like', "%{$request->warga}%");
        }
        if($request->agama != "Semua")
        {
            $q->where('agama', 'like', "%{$request->agama}%");
        }
        if($request->has('ayah'))
        {
            $q->where('nama_ayah', 'like', "%{$request->ayah}%");
        }
        if($request->has('ibu'))
        {
            $q->where('nama_ibu', 'like', "%{$request->ibu}%");
        }
        if($request->has('tmNikah'))
        {
            $q->where('tempat_nikah', 'like', "%{$request->tmNikah}%");
        }
        if($request->has('bkNikah'))
        {
            $q->where('no_buku_nikah', 'like', "%{$request->bkNikah}%");
        }
        if($request->has('nikahAwal'))
        {
            $nkStart = date("Y-m-d", strtotime($request->nikahAwal));
            $q->where('tanggal_nikah', '>', $nkStart);
        }
        if($request->has('nikahAkhir'))
        {
            $nkEnd = date("Y-m-d", strtotime($request->nikahAkhir));
            $q->where('tanggal_nikah', '<', $nkEnd);
        }
        if($request->kelurahan != "Semua")
        {
            $q->where('kartu_keluargas.kelurahan', 'like', "%{$request->kelurahan}%");
        }
        if($request->has('alamat'))
        {
            $q->where('kartu_keluargas.alamat', 'like', "%{$request->alamat}%");
        }
        if($request->has('rt'))
        {
            $q->where('kartu_keluargas.rt', 'like', "%{$request->rt}%");
        }
        if($request->has('rw'))
        {
            $q->where('kartu_keluargas.rw', 'like', "%{$request->rw}%");
        }
        if($request->has('status'))
        {
            $q->where('status', '=', $request->status);
        }
        $x = $q->get();
        $count = $q->count();


        return view('admin.data_kelahiran.print')
            ->with('q', $x)
            ->with('filter', $request)
            ->with('count', $count)
            ->with('typeLaporan', "Data Kelahiran");
    }

    /**
     * Retrieve kelahiran by query via ajax for datatables 
     *
     * @param   datatable default JSON output
     * @return  datatable JSON input
        int draw //page
        int recordsTotal //total records in database
        int recordsFiltered //
        string error (optional) //error message
     */
    public function getDatatableKelahirans(Request $request){
    $kelahirans = DB::table('kelahirans')->select(['kelahirans.id', 'no_skk', 'nama', 'waktu_kelahiran', 'tempat_lahir', 'agama', 'jenis_kelamin', 'kewarganegaraan', 'nama_ibu', 'nama_ayah', 'tempat_nikah', 'tanggal_nikah', 'no_buku_nikah', 'kelahirans.created_at', 'id_kk', 'kartu_keluargas.kelurahan', 'alamat', 'rt', 'rw', 'status'])
        ->join('kartu_keluargas', 'kartu_keluargas.id', '=', 'kelahirans.id_kk');
        
      return DataTables::of($kelahirans)
        ->filter(function($q) use ($request){
            if($request->has('noSkk'))
            {
                $q->where('no_skk', 'like', "%{$request->noSkk}%");
            }
            if($request->has('nama'))
            {
                $q->where('nama', 'like', "%{$request->nama}%");
            }
            if($request->has('tglAwal'))
            {
                $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
                $q->where('kelahirans.created_at', '>', $start);
            }
            if($request->has('tglAkhir'))
            {
                $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
                $q->where('kelahirans.created_at', '<', $end);
            }
            if($request->has('tempatLahir'))
            {
                $q->where('tempat_lahir', 'like', "%{$request->tempatLahir}%");
            }
            if($request->has('lhrAwal'))
            {
                $lStart = date("Y-m-d 00:00:00", strtotime($request->lhrAwal));
                $q->where('waktu_kelahiran', '>', $lStart);
            }
            if($request->has('lhrAkhir'))
            {
                $lEnd = date("Y-m-d 23:59:59", strtotime($request->lhrAkhir));
                $q->where('waktu_kelahiran', '<', $lEnd);
            }
            if($request->jk != "Semua")
            {
                $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
            }
            if($request->warga != "Semua")
            {
                $q->where('kewarganegaraan', 'like', "%{$request->warga}%");
            }
            if($request->agama != "Semua")
            {
                $q->where('agama', 'like', "%{$request->agama}%");
            }
            if($request->has('ayah'))
            {
                $q->where('nama_ayah', 'like', "%{$request->ayah}%");
            }
            if($request->has('ibu'))
            {
                $q->where('nama_ibu', 'like', "%{$request->ibu}%");
            }
            if($request->has('tmNikah'))
            {
                $q->where('tempat_nikah', 'like', "%{$request->tmNikah}%");
            }
            if($request->has('bkNikah'))
            {
                $q->where('no_buku_nikah', 'like', "%{$request->bkNikah}%");
            }
            if($request->has('nikahAwal'))
            {
                $nkStart = date("Y-m-d", strtotime($request->nikahAwal));
                $q->where('tanggal_nikah', '>', $nkStart);
            }
            if($request->has('nikahAkhir'))
            {
                $nkEnd = date("Y-m-d", strtotime($request->nikahAkhir));
                $q->where('tanggal_nikah', '<', $nkEnd);
            }
            if($request->kelurahan != "Semua")
            {
                $q->where('kartu_keluargas.kelurahan', 'like', "%{$request->kelurahan}%");
            }
            if($request->has('alamat'))
            {
                $q->where('kartu_keluargas.alamat', 'like', "%{$request->alamat}%");
            }
            if($request->has('rt'))
            {
                $q->where('kartu_keluargas.rt', 'like', "%{$request->rt}%");
            }
            if($request->has('rw'))
            {
                $q->where('kartu_keluargas.rw', 'like', "%{$request->rw}%");
            }
            if($request->has('status') && $request->status != "Semua")
            {
                $q->where('status', '=', $request->status);
            }
        })
        ->addColumn('action', function($k){
            if($k->status == 0){
                $s0 = "<a href='".url("admin/kelahiran")."/".$k->id."/pending_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Tunda' disabled><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/kelahiran")."/".$k->id."/process_document' class='btn btn-xs btn-icon btn-warning' data-toggle='tooltip' title='' data-original-title='Proses'><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/kelahiran")."/".$k->id."/finish_document' class='btn btn-xs btn-icon btn-success' data-toggle='tooltip' title='' data-original-title='Selesai'><i class='fa fa-check'></i></a>";
            }
            if($k->status == 1){
                $s0 = "<a href='".url("admin/kelahiran")."/".$k->id."/pending_document' class='btn btn-xs btn-icon btn-danger' data-toggle='tooltip' title='' data-original-title='Tunda'><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/kelahiran")."/".$k->id."/process_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Proses' disabled><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/kelahiran")."/".$k->id."/finish_document' class='btn btn-xs btn-icon btn-success' data-toggle='tooltip' title='' data-original-title='Selesai'><i class='fa fa-check'></i></a>";
            }
            if($k->status == 2){
                $s0 = "<a href='".url("admin/kelahiran")."/".$k->id."/pending_document' class='btn btn-xs btn-icon btn-danger' data-toggle='tooltip' title='' data-original-title='Tunda'><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/kelahiran")."/".$k->id."/process_document' class='btn btn-xs btn-icon btn-warning' data-toggle='tooltip' title='' data-original-title='Proses'><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/kelahiran")."/".$k->id."/finish_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Selesai' disabled><i class='fa fa-check'></i></a>";
            }            
            return("$s0 $s1 $s2
                <a href='".url('admin/kelahiran')."/".$k->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$k->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data' style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>
            ");
        })
        ->addColumn('alamat', function($k){
            $kk = KartuKeluarga::find($k->id_kk);
            return($kk->alamat.' RT '.$kk->rt.' RW '.$kk->rw);
        })
        ->addColumn('kelurahan', function($k){
            $kk = KartuKeluarga::find($k->id_kk);
            return($kk->kelurahan);
        })
        ->editColumn('nama', function($k){
            if($k->jenis_kelamin == "Laki - Laki")
                $jk = "L";
            else
                $jk = "P";
            return($k->nama.' ('.$jk.')');
        })
        ->editColumn('waktu_kelahiran', function($k){
            return date("d/m/Y", strtotime($k->waktu_kelahiran));
        })
        ->editColumn('status', function($k){
            if($k->status == 0)
                return("<span class='label label-danger'>Ditunda</span>");
            if($k->status == 1)
                return("<span class='label label-warning'>Diproses</span>");
            if($k->status == 2)
                return("<span class='label label-success'>Selesai</span>");
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    }

    public function pendingDoc($id)
    {      
        $kelahiran = Kelahiran::find($id);
        $this->logAction->action = "Update Status Data Kelahiran [Tunda] - ".$kelahiran->no_skk;

        try{
            $kelahiran->status = 0;
            $kelahiran->save();
            $this->logAction->item_id = $kelahiran->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('kelahiran.index');
    }

    public function processDoc($id)
    {
        $kelahiran = Kelahiran::find($id);
        $this->logAction->action = "Update Status Data Kelahiran [Proses] - ".$kelahiran->no_skk;

        try{
            $kelahiran->status = 1;
            $kelahiran->save();
            $this->logAction->item_id = $kelahiran->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('kelahiran.index');
    }

    public function finishDoc($id)
    {
        $kelahiran = Kelahiran::find($id);
        $this->logAction->action = "Update Status Data Kelahiran [Selesai] - ".$kelahiran->no_skk;

        try{
            $kelahiran->status = 2;
            $kelahiran->save();
            $this->logAction->item_id = $kelahiran->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('kelahiran.index');
    }
}
