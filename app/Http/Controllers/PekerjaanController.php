<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\ActionLog;
use App\Pekerjaan;
use DB;
use DataTables;

class PekerjaanController extends Controller
{
    private $logAction;

    public function __construct()
    {

      $this->logAction = New ActionLog;
      $this->logAction->id_user = 1; //should be changed
      $this->logAction->group = "Pekerjaan";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pekerjaan = Pekerjaan::all();

        return view('admin.pekerjaan.list')
            ->with('pekerjaan', $pekerjaan);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pekerjaan = New Pekerjaan;
        $title = "Tambah Jenis Pekerjaan";
        $action = "jenis_pekerjaan.store";

        return view('admin.pekerjaan.form')
            ->with('pekerjaan', $pekerjaan)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pekerjaan = new Pekerjaan;        

        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
        ]);        
        
        $pekerjaan->kode = $request->kode;
        $pekerjaan->nama = $request->nama;

        $this->logAction->action = "Tambah Jenis Pekerjaan- ".$request->kode;
        
        try{
            $pekerjaan->save();
            $this->logAction->item_id = $pekerjaan->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('jenis_pekerjaan.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        $title = "Tambah Jenis Pekerjaan";
        $action = "jenis_pekerjaan.update";

        return view('admin.pekerjaan.form')
            ->with('pekerjaan', $pekerjaan)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pekerjaan = Pekerjaan::find($id);        

        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
        ]);        
        
        $pekerjaan->kode = $request->kode;
        $pekerjaan->nama = $request->nama;

        $this->logAction->action = "Edit Jenis Pekerjaan- ".$request->kode;
        
        try{
            $pekerjaan->save();
            $this->logAction->item_id = $pekerjaan->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('jenis_pekerjaan.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pekerjaan = Pekerjaan::find($id);

        $this->logAction->action = "Hapus Jenis Pekerjaan - ".$pekerjaan->kode;

        try{
            $this->logAction->item_id = $pekerjaan->id; 
            Pekerjaan::destroy($id);
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('jenis_pekerjaan.index');
    }
    
    /**
     * Retrieve kk by query via ajax, limit : 5 
     *
     * @param  _POST  query
     * @return kk list json
     */
    public function getPekerjaan(){
        $pekerjaans = Pekerjaan::where('kode', 'like', '%'.$_GET['q'].'%')
                ->orWhere('nama', 'like', '%'.$_GET['q'].'%')
                ->limit(5)->get();

        return response()->json($pekerjaans);
    }

    public function getDatatablePekerjaans(Request $request){
      $pekerjaan = DB::table('pekerjaans')->select(['id', 'kode', 'nama']);

      return DataTables::of($pekerjaan)
        ->addColumn('action', function($p){
            return("
                <a href='".url('admin/master/jenis_pekerjaan')."/".$p->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$p->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data'style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>");
        })
        ->make(true);
    }
}
