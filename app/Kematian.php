<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kematian extends Model
{
    protected $fillable = [
			'no_skk',
			'nama',
			'id_kk',
			'id_ktp',
			'waktu_kematian'
		];



	public $timesteamps = false;

	protected $table = 'kematians';
}
