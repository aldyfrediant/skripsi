<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
	protected $table = 'pekerjaans';

	public function getPekerjaans($queriesArray, $limit, $offset){
		$tempQuery = DB::table($this->table);
		foreach($queriesArray as $key => $query){
            //build search queries
            if($query != ''){
	            if($key=='kode') {
					$tempQuery->where('kode', 'like', '%'.$query.'%');
				}
				else if($key=='nama') {
					$tempQuery->where('nama', 'like', '%'.$query.'%');
				} 
			}
        }
        
        $data['sql'] = $tempQuery->toSql();
        $data['count'] = $tempQuery->count();
        $data['result'] = $tempQuery->limit($limit)->offset($offset)->get();
        return $data;
    }
}
