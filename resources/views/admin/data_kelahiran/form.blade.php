@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2-bootstrap.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/bootstrap-datetimepicker/moment.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/select2/select2.full.min.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Data Kependudukan</li>
                                <li>
                                    <a class="link-effect" href="{{URL::to('admin/kelahiran')}}">Data Kelahiran</a>
                                </li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Data Keluarga</h3>
                                </div>
                                <div class="block-content">
                                @if($action=='kelahiran.store')
                                  {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                @else
                                  {!! Form::open(['route' => [$action, $kelahiran->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                @endif
                                        <div class="form-group">
                                            <h4 class="form-subtitle">Data Anak</h4>
                                            <div class="col-xs-12">
                                                <div class="form-material floating">
                                                    {!! Form::text('noSkk', $kelahiran->no_skk, ['class' => 'form-control']) !!}
                                                    <label for="noSkk">No. Surat Keterangan Kelahiran</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('nama', $kelahiran->nama, ['class' => 'form-control']) !!}
                                                    <label for="nama">Nama Anak</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('tempatLahir', $kelahiran->tempat_lahir, ['class' => 'form-control']) !!}
                                                    <label for="tempatLahir">Tempat Lahir</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-3">
                                                <div class="form-material date" data-show-today-button="true" data-show-clear="true" data-show-close="true" data-side-by-side="true">
                                                    {!! Form::text('waktuLahir', date("m-d-Y H:i A", strtotime($kelahiran->waktu_kelahiran)), ['class' => 'form-control', 'id' => 'waktuLahir']) !!}
                                                    {{-- <input class="form-control" type="text" id="waktuLahir" name="waktuLahir" value="{{ date("m-d-Y H:i A", strtotime($kelahiran->waktu_kelahiran)) }}"> --}}
                                                    <label for="waktuLahir">Waktu Kelahiran</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-material floating">
                                                    {!! Form::select('agama', [
                                                        'Islam' => 'Islam',
                                                        'Kristen' => 'Kristen',
                                                        'Katolik' => 'Katolik',
                                                        'Hindu' => 'Hindu',
                                                        'Budha' => 'Budha',
                                                        'Lainnya' => 'Lainnya',
                                                        ], $kelahiran->agama, ['class' => 'form-control']) !!}
                                                    <label>Agama</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-material floating">
                                                    {!! Form::select('jk', ['Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], $kelahiran->jenis_kelamin, ['class' => 'form-control']) !!}
                                                    <label>Jenis Kelamin</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="form-material floating">
                                                    {!! Form::select('kewarganegaraan', ['WNI'=>'WNI','WNA'=>'WNA'], $kelahiran->kewarganegaraan, ['class' => 'form-control']) !!}
                                                    <label>Kewarganegaraan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h4 class="form-subtitle">Data Orang Tua</h4>
                                            <div class="col-xs-12">
                                                <div class="form-material floating">
                                                    @if($action=='kelahiran.store')
                                                        {!! Form::select('idKK', array(), $kelahiran->id_kk, ['class' => 'form-control kkList', 'id' => 'ListKK']) !!}
                                                    @else
                                                    @php
                                                        $markupKK = "( ".$dataKK->no_kk." ) ". $dataKK->kepala_keluarga." - ".$dataKK->alamat." RT ".$dataKK->rt." RW ".$dataKK->rw." Kel. ".$dataKK->kelurahan;
                                                    @endphp
                                                        {!! Form::select('idKK', [$kelahiran->id_kk => $markupKK], $kelahiran->id_kk, ['class' => 'form-control kkList']) !!}
                                                    @endif
                                                    <label for="idKK">Pilih Data Kartu Keluarga</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('namaAyah', $kelahiran->nama_ayah, ['class' => 'form-control']) !!}{{-- 
                                                    @if($action=='kelahiran.store')
                                                        {!! Form::select('namaAyah', array(), $kelahiran->nama_ayah, ['class' => 'form-control individuList']) !!}
                                                    @else
                                                        {!! Form::select('namaAyah', [$kelahiran->nama_ayah => $dataAyah->nama], $kelahiran->nama_ayah, ['class' => 'form-control individuList']) !!}
                                                    @endif --}}
                                                    <label for="namaAyah">Nama Ayah</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('namaIbu', $kelahiran->nama_ibu, ['class' => 'form-control']) !!}
                                                    {{-- @if($action=='kelahiran.store')
                                                        {!! Form::select('namaIbu', array(), $kelahiran->nama_ibu, ['class' => 'form-control individuList']) !!}
                                                    @else
                                                        {!! Form::select('namaIbu', [$kelahiran->nama_ibu => $dataIbu->nama], $kelahiran->nama_ibu, ['class' => 'form-control individuList']) !!}
                                                    @endif --}}
                                                    <label for="namaIbu">Nama Ibu</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::text('tempatNikah', $kelahiran->tempat_nikah, ['class' => 'form-control']) !!}
                                                    <label for="tempatNikah">Tempat Pernikahan</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">                                          
                                                    <input class="datepicker form-control" type="text" id="tanggalNikah" name="tanggalNikah" data-date-format="dd/mm/yy" value="{{ $kelahiran->tanggal_nikah }}">
                                                    <label for="tanggalNikah">Tanggal Pernikahan</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::text('noBukuNikah', $kelahiran->no_buku_nikah, ['class' => 'form-control']) !!}
                                                    <label for="noBukuNikah">No. Buku Nikah</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit">
                                                    <i class="fa fa-plus push-5-r"></i> Register Data
                                                </button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

<script>
$(document).ready(function() {
    $('#waktuLahir').datetimepicker({
        sideBySide: true,
        format: 'MM/DD/YYYY HH:mm'
    });
    
    $('.datepicker').datepicker({
    format: 'dd-mm-yyyy'
    });

    function formatKK (repo) {
        if (repo.loading) return "Searching...";

        var markup = "<div>( "+repo.no_kk+" ) "+ repo.kepala_keluarga+" - "+repo.alamat+" RT "+repo.rt+" RW "+repo.rw+" Kel. "+repo.kelurahan+"</div>";

        return markup;
    }

    function formatKKSelection (repo) {
        return repo.text || "( "+repo.no_kk+" ) "+ repo.kepala_keluarga+" - "+repo.alamat+" RT "+repo.rt+" RW "+repo.rw+" Kel. "+repo.kelurahan;
    }

     /* Select 2 Kartu Keluarga*/
      $(".kkList").select2({
        placeholder: " ",
        allowClear: true,

        ajax: {
          url: "{{ action('KartuKeluargaController@getKKs') }}",
          dataType: 'json',
          delay: 2,
          type: "get",
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data, params) {
            return {
              results: data
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: formatKK,
        templateSelection: formatKKSelection
      });
      /*End select 2 Kartu Keluarga*/

      /* Select 2 Individu*/
      $(".individuList").select2({
        placeholder: " ",
        allowClear: true,

        ajax: {
          url: "{{ action('PendudukController@getIndividu') }}",
          dataType: 'json',
          delay: 2,
          type: "get",
          data: function (params) {
            return {
              q: params.term,
              kk: document.getElementById('ListKK').value
            };
          },
          processResults: function (data, params) {
            return {
              results: data
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: function(repo) {
          if (repo.loading) return repo.nik;
          var markup = "<div>"+repo.nik+" - "+repo.nama+"</div>";
          return markup;
        },
        templateSelection: function(repo) {
          return repo.text || repo.nik+" - "+ repo.nama;
        }

      });
      /*End select 2 Kartu Keluarga*/
});
</script>
@endsection