@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">
        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Catatan Aksi Yang Dilakukan
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Home</li>
                                <li><a class="link-effect" href="{{URL::to('admin/action_logs')}}">Catatan Aksi</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Aksi Dilakukan</th>
                                        <th>Group</th>
                                        <th width="250px">Waktu</th>
                                    </tr>
                                </thead>

                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

    {!! Form::open(['method' => 'delete',
        'route' => ['data_penduduk.destroy', null],
        'class' => 'hiddenDeleteForm'])
    !!}
    {!! Form::close() !!}

  <script type="text/javascript">
    $(document).ready(function() {
        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "id_user" },
                { "data": "action" },
                { "data": "group" },
                { "data": "created_at" },
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('ActionLogController@getDatatableLogs') }}',
              "type": "POST",
              "data": {
                  @foreach(app('request')->input() as $key => $query)
                    @if ($query!='')
                      "{{ $key }}": "{{ $query }}",
                    @endif
                  @endforeach
              }
            },
            deferRender: true
        });
    });
  </script>
@endsection