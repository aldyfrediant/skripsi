@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2-bootstrap.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/bootstrap-datetimepicker/moment.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/select2/select2.full.min.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Data Kependudukan</li>
                                <li>
                                    <a class="link-effect" href="{{URL::to('admin/kematian')}}">Data Kematian</a>
                                </li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Data Keluarga</h3>
                                </div>
                                <div class="block-content">
                                    @if ($errors->has('noSkk'))
                                        <div class="error">{{ $errors->first('noSkk') }}</div>
                                    @endif
                                    @if ($errors->has('idKK'))
                                        <div class="error">{{ $errors->first('idKK') }}</div>
                                    @endif
                                    @if ($errors->has('idKTP'))
                                        <div class="error">{{ $errors->first('idKTP') }}</div>
                                    @endif
                                    @if ($errors->has('waktuKematian'))
                                        <div class="error">{{ $errors->first('waktuKematian') }}</div>
                                    @endif
                                    @if($action=='kematian.store')
                                      {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                    @else
                                      {!! Form::open(['route' => [$action, $kematian->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                    @endif
                                        <div class="form-group">
                                            <h4 class="form-subtitle">Data Warga</h4>
                                            <div class="col-xs-12">
                                                <div class="form-material floating">
                                                    <input value="{{ $kematian->no_skk }}" class="form-control" type="text" id="noSkk" name="noSkk">
                                                    <label for="noSkk">No. Surat Keterangan kematian</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material floating">
                                                    @if($action=='kematian.store')
                                                        {!! Form::select('idKK', array(), $kematian->id_kk, ['class' => 'form-control kkList', 'id' => 'ListKK']) !!}
                                                    @else
                                                        @php
                                                            $markupKK = "( ".$dataKK->no_kk." ) ". $dataKK->kepala_keluarga." - ".$dataKK->alamat." RT ".$dataKK->rt." RW ".$dataKK->rw." Kel. ".$dataKK->kelurahan;
                                                        @endphp
                                                        {!! Form::select('idKK', [$kematian->id_kk => $markupKK], $kematian->id_kk, ['class' => 'form-control kkList', 'id' => 'ListKK']) !!}
                                                    @endif
                                                    <label for="idKK">Pilih Data Kartu Keluarga</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    @if($action=='kematian.store')
                                                        {!! Form::select('idKTP', array(), $kematian->id_ktp, ['class' => 'form-control individuList']) !!}
                                                    @else
                                                        {!! Form::select('idKTP', [$kematian->id_ktp => $dataKTP->nama], $kematian->id_ktp, ['class' => 'form-control individuList']) !!}
                                                    @endif
                                                    <label for="idKK">Pilih Data Penduduk</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                    <div class="form-material date" data-show-today-button="true" data-show-clear="true" data-show-close="true" data-side-by-side="true">
                                                    <input class="form-control" type="text" id="waktuKematian" name="waktuKematian" value="{{ date("Y-m-d H:i:s", strtotime($kematian->waktu_kematian)) }}">
                                                    <label for="waktuKematian">Waktu kematian</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit">
                                                    <i class="fa fa-plus push-5-r"></i> Register Data
                                                </button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

<script>
$(document).ready(function() {
    $('#waktuKematian').datetimepicker({
        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm:ss'
    });

    function formatKK (repo) {
        if (repo.loading) return "Searching...";

        var markup = "<div>( "+repo.no_kk+" ) "+ repo.kepala_keluarga+" - "+repo.alamat+" RT "+repo.rt+" RW "+repo.rw+" Kel. "+repo.kelurahan+"</div>";

        return markup;
    }

    function formatKKSelection (repo) {
        return repo.text || "( "+repo.no_kk+" ) "+ repo.kepala_keluarga+" - "+repo.alamat+" RT "+repo.rt+" RW "+repo.rw+" Kel. "+repo.kelurahan;
    }

     /* Select 2 Kartu Keluarga*/
      $(".kkList").select2({
        placeholder: " ",
        allowClear: true,

        ajax: {
          url: "{{ action('KartuKeluargaController@getKKs') }}",
          dataType: 'json',
          delay: 2,
          type: "get",
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data, params) {
            return {
              results: data
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: formatKK,
        templateSelection: formatKKSelection
      });
      /*End select 2 Kartu Keluarga*/

      /* Select 2 Individu*/
      $(".individuList").select2({
        placeholder: " ",
        allowClear: true,

        ajax: {
          url: "{{ action('PendudukController@getIndividu') }}",
          dataType: 'json',
          delay: 2,
          type: "get",
          data: function (params) {
            return {
              q: params.term,
              kk: document.getElementById('ListKK').value
            };
          },
          processResults: function (data, params) {
            return {
              results: data
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: function(repo) {
          if (repo.loading) return repo.nik;
          var markup = "<div>"+repo.nik+" - "+repo.nama+" - "+repo.tanggal_lahir+"</div>";
          return markup;
        },
        templateSelection: function(repo) {
          return repo.text || repo.nik+" - "+ repo.nama + " - " +repo.tanggal_lahir;
        }

      });
      /*End select 2 Kartu Keluarga*/
});
</script>
@endsection