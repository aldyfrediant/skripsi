@if(Auth::user()->role == 'Admin')
@extends('admin.layouts.app')

@section('content')
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Home</li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Data User</h3>
                                </div>
                                <div class="block-content">
                                @if($action=='user_management.store')
                                  {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                @else
                                  {!! Form::open(['route' => [$action, $user->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                @endif
                                {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::text('name', $user->name, ['class'=>'form-control']) !!}
                                                    <label for="name">Nama</label>
                                                @if($errors->has('name'))
                                                {{$errors->first('name')}}
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
                                                    <label for="email">Email</label>
                                                @if($errors->has('email'))
                                                {{$errors->first('email')}}
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::select('role',['Admin' => 'Admin', 'Operator' => 'Operator'], $user->role, ['class' => 'form-control', 'placeholder' => '']) !!}
                                                    <label>Role</label>
                                                @if($errors->has('role'))
                                                {{$errors->first('role')}}
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::text('username', $user->username, ['class' => 'form-control']) !!}
                                                    <label for="username">Username</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                                    <label>Password</label>
                                                </div>
                                                @if($errors->has('password'))
                                                {{$errors->first('password')}}
                                                @endif
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                                    <label>Ulangi Password</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-plus push-5-r"></i> Register Data</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
@endsection
@else
@php 
    dd('Anda Tidak Memiliki Hak Akses Sebagai Admin!');
    exit;
@endphp
@endif