@extends('admin.layouts.print.app')

@section('printBody')
  <div class="row">
    <div class="col-xs-12">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-4">
          <h5>
            Periode: 
              @if(isset($filter->tglAwal)){{ date("d/m/Y", strtotime($filter->tglAwal)) }} @else *  @endif
              -
              @if(isset($filter->tglAkhir)){{ date("d/m/Y", strtotime($filter->tglAkhir)) }} @else * @endif
          </h5>
        </div>
        @if(isset($filter->kelurahan) && $filter->kelurahan != "Semua")
        <div class="col-xs-4">
          <h5>Kelurahan: {{ $filter->kelurahan }}</h5>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5 style="font-weight: bold;">Jumlah: {{ $count }} Data </h5>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered table-striped">
              <thead>
                  <tr>
                      <th>No. Kartu Keluarga</th>
                      <th>Nama Kepala Keluarga</th>
                      <th>Alamat</th>
                      <th>Tanggal Registrasi</th>
                      <th>Kelurahan</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($kk as $data)
                @php $d = date('d/m/Y', strtotime($data->created_at)); @endphp
                <tr>
                  <td>{{ $data->no_kk }}</td>
                  <td>{{ $data->kepala_keluarga}}</td>
                  <td>{{ $data->alamat.' RT '.$data->rt.' RW '.$data->rw}}</td>
                  <td>{{ $d }}</td>
                  <td>{{ $data->kelurahan }}</td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
	
@endsection