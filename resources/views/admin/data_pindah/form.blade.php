@extends('admin.layouts.app')

@section('content')
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Data Kependudukan</li>
                                <li>
                                    @if($type == "datang")
                                        <a class="link-effect" href="{{URL::to('admin/pindah/datang')}}">Data Pindah Datang</a>
                                    @else
                                        <a class="link-effect" href="{{URL::to('admin/pindah/pergi')}}">Data Pindah Pergi</a>
                                    @endif
                                </li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Data Pindah {{$type}}</h3>
                                </div>
                                <div class="block-content">
                                    @if($type == 'datang')
                                        @if($action=='datang.store')
                                          {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                        @else
                                          {!! Form::open(['route' => [$action, $data->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                        @endif
                                    @else                                
                                        @if($action=='pergi.store')
                                          {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                        @else
                                          {!! Form::open(['route' => [$action, $data->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                        @endif
                                    @endif
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('noreg', $data->no_registrasi, ['class' => 'form-control']) !!}
                                                    <label for="noreg">No. Registrasi</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('noKK', $data->no_kk, ['class' => 'form-control']) !!}
                                                    <label for="noKK">No. Kartu Keluarga</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::text('nik', $data->nik, ['class' => 'form-control']) !!}
                                                    <label for="nik">NIK</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::text('nama', $data->nama, ['class' => 'form-control']) !!}
                                                    <label for="nama">Nama Penduduk</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">                                                    
                                                    {!! Form::select('kelurahan', [
                                                        'Ancol' => 'Ancol',
                                                        'Balonggede' => 'Balonggede',
                                                        'Ciateul' => 'Ciateul',
                                                        'Cigereleng' => 'Cigereleng',
                                                        'Ciseureuh' => 'Ciseureuh',
                                                        'Pasirluyu' => 'Pasirluyu',
                                                        'Pungkur' => 'Pungkur'], $data->kelurahan, ['class' => 'form-control']) !!}
                                                    <label for="kelurahan">Kelurahan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::select('jk', ['Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], $data->jenis_kelamin, ['class' => 'form-control']) !!}
                                                    <label>Jenis Kelamin</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::select('agama', [
                                                        'Islam' => 'Islam',
                                                        'Kristen' => 'Kristen',
                                                        'Katolik' => 'Katolik',
                                                        'Hindu' => 'Hindu',
                                                        'Budha' => 'Budha',
                                                        'Lainnya' => 'Lainnya',
                                                        ], $data->agama, ['class' => 'form-control']) !!}
                                                    <label>Agama</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::select('kewarganegaraan', ['WNI'=>'WNI','WNA'=>'WNA'], $data->kewarganegaraan, ['class' => 'form-control']) !!}
                                                    <label>Kewarganegaraan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material floating">
                                                    {!! Form::text('alamatLama', $data->alamat_lama, ['class' => 'form-control']) !!}
                                                    <label for="alamatLama">Alamat Lama</label>
                                                </div>
                                            </div>
                                        </div>
                                        @if($type == "datang")
                                        <div class="form-group">                                            
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('kotaAsal', $data->kota_asal, ['class' => 'form-control']) !!}
                                                    <label for="kotaAsal">Kota Asal</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('kecAsal', $data->kecamatan_asal, ['class' => 'form-control']) !!}
                                                    <label for="kecAsal">Kecamatan Asal</label>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group">                                            
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('kotaTujuan', $data->kota_tujuan, ['class' => 'form-control']) !!}
                                                    <label for="kotaTujuan">Kota Tujuan</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('kecTujuan', $data->kecamatan_tujuan, ['class' => 'form-control']) !!}
                                                    <label for="kecTujuan">Kecamatan Tujuan</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material floating">
                                                    {!! Form::text('alamatBaru', $data->alamat_baru, ['class' => 'form-control']) !!}
                                                    <label for="alamatBaru">Alamat Baru</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit">
                                                    <i class="fa fa-plus push-5-r"></i> Register Data
                                                </button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
@endsection