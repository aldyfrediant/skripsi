@extends('admin.layouts.app')

@section('content')
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                Change Password
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li><a class="link-effect" href="{{URL::to('/')}}">Home</a></li>
                                <li>Change Password</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="block-content">
                                  {!! Form::open(['action' => 'UserController@updatePass', 'role' => 'form', 'method' => 'PUT','class' => 'form-horizontal push-10-t push-10']) !!}
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                                    <label>Password Baru</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                                    <label>Ulangi Password Baru</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-plus push-5-r"></i> Register Data</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
@endsection