@if(Auth::user()->role == 'Admin')
@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">
        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Laporan</li>
                                <li>
                                    <a class="link-effect" href="{{URL::to('admin/rekap')}}">Rekapitulasi</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Pilihan Data Rekapitulasi</h3>
                                </div>
                                <div class="block-content">
                                    {!! Form::open(['action' => 'RekapController@print', 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                        <div class="form-group">
                                            <div class="col-xs-4"> 
                                                <div class="form-material floating">                                           
                                                    {!! Form::text('from', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'required']) !!}
                                                    <label for="from">Awal Periode</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">                                       
                                                    {!! Form::text('to', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'required']) !!}
                                                    <label for="to">Akhir Periode</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::select('type', ['Jenis Kelamin' => 'Jenis Kelamin', 'Agama' => 'Agama', 'Kewarganegaraan' => 'Kewarganegaraan'], null, ['placeholder' => 'Kategori', 'class' => 'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit">
                                                    <i class="fa fa-print push-5-r"></i> Print Data
                                                </button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

<script>
$(document).ready(function() {
    $('.datepicker').datepicker({
    format: 'dd-mm-yyyy'
    });
});
</script>
@endsection
@else
    @php
    dd('Anda Tidak Memiliki Hak Akses Sebagai Admin!');
    exit;
    @endphp
@endif