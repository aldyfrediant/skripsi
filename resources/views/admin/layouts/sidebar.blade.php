
        <!-- Page Container -->
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
           <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <a class="h5 text-white" href="index.html">
                                <span class="h4 font-w600 sidebar-mini-hide">SIDK Kec. Regol</span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                <li>
                                    <a href="{{URL::to('/')}}"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Data Management</span></li>                              
                                @if(Auth::user()->role == 'Admin')
                                <li>
                                    <a href="{{URL::to('admin/user_management')}}"><i class="si si-users"></i><span class="sidebar-mini-hide">User Management</span></a>
                                </li>
                                @endif
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-folder-o"></i><span class="sidebar-mini-hide">Master Data</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{URL::to('admin/master/kartu_keluarga')}}">Kartu Keluarga</a>
                                        </li>
                                        <li>
                                            <a href="{{URL::to('admin/master/jenis_pekerjaan')}}">Jenis Pekerjaan</a>
                                        </li>
                                        <li>
                                            <a href="{{URL::to('admin/master/data_penduduk')}}">Data Penduduk</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Data Kependudukan</span></li>
                                <li>
                                    <a href="{{URL::to('admin/kelahiran')}}"><i class="fa fa-plus"></i><span class="sidebar-mini-hide">Data Kelahiran</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::to('admin/kematian')}}"><i class="fa fa-minus"></i><span class="sidebar-mini-hide">Data Kematian</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::to('admin/pindah/datang')}}"><i class="fa fa-arrow-left"></i><span class="sidebar-mini-hide">Data Pindah Datang</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::to('admin/pindah/pergi')}}"><i class="fa fa-arrow-right"></i><span class="sidebar-mini-hide">Data Pindah Pergi</span></a>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Laporan &amp; Catatan</span></li>
                                @if(Auth::user()->role == 'Admin')
                                <li>
                                    <a href="{{URL::to('admin/rekap')}}"><i class="fa fa-database"></i><span class="sidebar-mini-hide">Rekapitulasi Data</span></a>
                                </li>
                                @endif
                                <li>
                                    <a href="{{URL::to('admin/action_logs')}}"><i class="fa fa-list-alt"></i><span class="sidebar-mini-hide">Catatan Aksi</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->