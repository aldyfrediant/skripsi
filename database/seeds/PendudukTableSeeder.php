<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class PendudukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dpL = ['Asep', 'Rahmat', 'Udin', 'Rangga', 'Muhammad', 'Cecep', 'Maman', 'Rusli', 'Airlangga', 'Aldy', 'Ramdhani', 'Imam', 'Iman', 'Kusnadi', 'Ari', 'Andi', 'Kuncoro', 'Jajang', 'Yanto', 'Iyus', 'Rusman', 'Harry', 'Tubagus'];
        $bkL = ['Saepudin', 'Nurjaman', 'Ismail', 'Sutanto', 'Suratno', 'Nugraha', 'Nurjaman', 'Jaka', 'Rismanto', 'Utoro', 'Hartono', 'Priatna', 'Surasep', 'Tri Dharma', 'Yulianto', 'Fauzie', 'Eka', 'Wicaksono', 'Arief', 'Purwanto', 'Kustiawan'];

        $dpLA = ['Michael', 'Steven', 'Marshall', 'Theodore', 'Barnabus', 'Shannon', 'Oliver', 'Christian', 'Marcell', 'Maximillian', 'Brillian', 'Jonathan', 'John', 'Joseph'];
        $bkLA = ['Dickson', 'Oracle', 'Ericksen', 'Aldrin', 'Armstrong', 'Stinson', 'Johnson', 'William', 'Mosby', 'Cooper', 'Hoftsader'];

        $dpP = ['Desma', 'Risma', 'Marissa', 'Anissa', 'Nurul', 'Maya', 'Siti', 'Novi', 'Lia', 'Mia', 'Nia', 'Kayra', 'Nadia', 'Nadya', 'Lily'];
        $bkP = ['Amalia', 'Putri', 'Eka', 'Ismail', 'Anastasia', 'Fajriati', 'Nurawalia', 'Ulfah', 'Pertiwi', 'Indah', 'Regina', 'Silviana', 'Agustina'];

        $dpPA = ['Loretta', 'Jenny', 'Mia', 'Sasha', 'Penny', 'Amy', 'Rihanna', 'Brie', 'Nicole'];
        $bkPA = ['Stinson', 'White', 'Blighe', 'Farah', 'Fowler', 'Bella', 'Kidman', 'Witherspoon', 'Jasmine'];

        $cI = ['Bandung', 'Jakarta', 'Bekasi', 'Tanggerang', 'Depok', 'Bogor', 'Surabaya', 'Denpasar'];

        $pendidikan = [
            'Tidak / Belum Sekolah',
            'Belum Tamat SD / Sederajat',
            'Tamat SD / Sederajat',
            'SLTP / Sederajat',
            'SLTA / Sederajat',
            'Diploma I / II',
            'Akademi / Diploma III / Sarjana Muda',
            'Diploma IV / Strata I',
            'Strata II',
            'Strata III'
        ];

        $hubungan = [
            'Kepala Keluarga',
            'Suami',
            'Istri',
            'Anak',
            'Menantu',
            'Cucu',
            'Orang Tua',
            'Mertua',
            'Famili Lain',
            'Pembantu',
            'Lainnya'
        ];

        $id_kk = DB::table('kartu_keluargas')->pluck('id')->all();
        $id_p = DB::table('pekerjaans')->pluck('id')->all();

        $faker = Faker::create();
        for ($i=1; $i <= 2000; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $born = $faker->dateTimeBetween($startDate = '-15000 days', $endDate = '-6570 days')->format('Y-m-d');
            $data[] = [
                'nik'                => $faker->numberBetween($min = 000001, $max = 010001),
                'nama'               => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'jenis_kelamin'      => 'Laki - Laki',
                'tempat_lahir'       => $faker->randomElement($cI),
                'tanggal_lahir'      => $born,
                'pendidikan'         => $faker->randomElement($pendidikan),
                'pekerjaan'          => $faker->randomElement($id_p),
                'agama'              => $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'status_pernikahan'  => $faker->randomElement(['Kawin', 'Belum Kawin', 'Cerai Hidup', 'Cerai Mati']),
                'status_hubungan'    => $faker->randomElement($hubungan),
                'kewarganegaraan'    => 'WNI',
                'nama_ayah'          =>$faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'nama_ibu'           =>$faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'created_at'         => $date, 
                'updated_at'         => $date,
                'id_kk'              => $faker->randomElement($id_kk)
            ];
        }

        for ($i=1; $i <= 2000; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $born = $faker->dateTimeBetween($startDate = '-15000 days', $endDate = '-6570 days')->format('Y-m-d');
            $data[] = [
                'nik'                => $faker->numberBetween($min = 020001, $max = 030001),
                'nama'               => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'jenis_kelamin'      => 'Perempuan',
                'tempat_lahir'       => $faker->randomElement($cI),
                'tanggal_lahir'      => $born,
                'pendidikan'         => $faker->randomElement($pendidikan),
                'pekerjaan'          => $faker->randomElement($id_p),
                'agama'              => $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'status_pernikahan'  => $faker->randomElement(['Kawin', 'Belum Kawin', 'Cerai Hidup', 'Cerai Mati']),
                'status_hubungan'    => $faker->randomElement($hubungan),
                'kewarganegaraan'    => 'WNI',
                'nama_ayah'          =>$faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'nama_ibu'           =>$faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'created_at'         => $date, 
                'updated_at'         => $date,
                'id_kk'              => $faker->randomElement($id_kk)
            ];
        }

        for ($i=1; $i <= 2000; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $born = $faker->dateTimeBetween($startDate = '-15000 days', $endDate = '-6570 days')->format('Y-m-d');
            $data[] = [
                'nik'                => $faker->numberBetween($min = 040001, $max = 050001),
                'nama'               => $faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'jenis_kelamin'      => 'Laki - Laki',
                'tempat_lahir'       => $faker->randomElement($cI),
                'tanggal_lahir'      => $born,
                'pendidikan'         => $faker->randomElement($pendidikan),
                'pekerjaan'          => $faker->randomElement($id_p),
                'agama'              => $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'status_pernikahan'  => $faker->randomElement(['Kawin', 'Belum Kawin', 'Cerai Hidup', 'Cerai Mati']),
                'status_hubungan'    => $faker->randomElement($hubungan),
                'kewarganegaraan'    => 'WNA',
                'nama_ayah'          =>$faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'nama_ibu'           =>$faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
                'created_at'         => $date, 
                'updated_at'         => $date,
                'id_kk'              => $faker->randomElement($id_kk)
            ];
        }
        for ($i=1; $i <= 2000; $i++){
        	$date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
        	$born = $faker->dateTimeBetween($startDate = '-15000 days', $endDate = '-6570 days')->format('Y-m-d');
        	$data[] = [
                'nik'                => $faker->numberBetween($min = 060001, $max = 070001),
                'nama'	 			 => $faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
                'jenis_kelamin'		 => 'Perempuan',
                'tempat_lahir'		 => $faker->randomElement($cI),
                'tanggal_lahir'		 => $born,
                'pendidikan'		 => $faker->randomElement($pendidikan),
                'pekerjaan'		 	 => $faker->randomElement($id_p),
                'agama'				 =>	$faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'status_pernikahan'	 => $faker->randomElement(['Kawin', 'Belum Kawin', 'Cerai Hidup', 'Cerai Mati']),
                'status_hubungan'	 => $faker->randomElement($hubungan),
                'kewarganegaraan'	 => 'WNA',
                'nama_ayah'			 =>$faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'nama_ibu'			 =>$faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
        		'created_at' 		 => $date, 
        		'updated_at' 		 => $date,
        		'id_kk'				 => $faker->randomElement($id_kk)
        	];
        }
        DB::table('penduduks')->insert($data);
    }
}
