<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class PindahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   $dpL = ['Asep', 'Rahmat', 'Udin', 'Rangga', 'Muhammad', 'Cecep', 'Maman', 'Rusli', 'Airlangga', 'Aldy', 'Ramdhani', 'Imam', 'Iman', 'Kusnadi', 'Ari', 'Andi', 'Kuncoro', 'Jajang', 'Yanto', 'Iyus', 'Rusman', 'Harry', 'Tubagus'];
        $bkL = ['Saepudin', 'Nurjaman', 'Ismail', 'Sutanto', 'Suratno', 'Nugraha', 'Nurjaman', 'Jaka', 'Rismanto', 'Utoro', 'Hartono', 'Priatna', 'Surasep', 'Tri Dharma', 'Yulianto', 'Fauzie', 'Eka', 'Wicaksono', 'Arief', 'Purwanto', 'Kustiawan'];

        $dpLA = ['Michael', 'Steven', 'Marshall', 'Theodore', 'Barnabus', 'Shannon', 'Oliver', 'Christian', 'Marcell', 'Maximillian', 'Brillian', 'Jonathan', 'John', 'Joseph'];
        $bkLA = ['Dickson', 'Oracle', 'Ericksen', 'Aldrin', 'Armstrong', 'Stinson', 'Johnson', 'William', 'Mosby', 'Cooper', 'Hoftsader'];

        $dpP = ['Desma', 'Risma', 'Marissa', 'Anissa', 'Nurul', 'Maya', 'Siti', 'Novi', 'Lia', 'Mia', 'Nia', 'Kayra', 'Nadia', 'Nadya', 'Lily'];
        $bkP = ['Amalia', 'Putri', 'Eka', 'Ismail', 'Anastasia', 'Fajriati', 'Nurawalia', 'Ulfah', 'Pertiwi', 'Indah', 'Regina', 'Silviana', 'Agustina'];

        $dpPA = ['Loretta', 'Jenny', 'Mia', 'Sasha', 'Penny', 'Amy', 'Rihanna', 'Brie', 'Nicole'];
        $bkPA = ['Stinson', 'White', 'Blighe', 'Farah', 'Fowler', 'Bella', 'Kidman', 'Witherspoon', 'Jasmine'];

        $jalan1 = ['Balonggede', 'Ancol', 'Denki', 'Sriwijaya', 'Kembar', 'Kembar Dalam', 'Kembar I', 'Kembar II', 'Kembar IV', 'BKR', 'Pasir Salam', 'Pasirluyu', 'Srimahi', 'Mengger Girang', 'Ciburuy', 'Srisuci', 'Sriayu', 'Srielok', 'H. Samsudin', 'Pungkur', 'Moch. Ramdan', 'Ibu Inggit Garnasih'];

        $jalan2 = ['Fatmawati', 'Perjuangan', 'Nasional', 'Marakas', 'Tamborin', 'H. Udin', 'Turangga', 'H. Nudin', 'Delima', 'Lebakbulus', 'Karang Asri', 'Manunggal', 'Adhyaksa', 'Grogol', 'Cempaka Lestari I', 'Cempaka Lestari 2', 'Lebak Indah III', 'Cirendeu Permai'];

        $kota = ['Jakarta', 'Depok', 'Surabaya', 'Bekasi', 'Denpasar', 'Tangerang', 'Kebumen', 'Kediri'];
        $kec = ['Gombong', 'Ambal', 'Pejagoan', 'Petanahan', 'Padang Sambian', 'Tegal Harum', 'Tegal Kerta', 'Pemecutan', 'Dauh Puri', 'Beji', 'Cipayung', 'Sukmajaya', 'Cinere', 'Bantargebang', 'Cikiwul', 'Jatiraden', 'Benowo', 'Bubutan', 'Bulak'];
        $faker = Faker::create();
        for ($i=1; $i <= 700; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_registrasi'     => 'DTG'.$faker->unique()->numberBetween($min = 1, $max = 800),
                'nik'               => $faker->unique()->numberBetween($min = 9040001, $max = 9050000),
                'no_kk'             => $faker->unique()->numberBetween($min = 32739010001, $max = 32739030000),
                'nama'              => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'kota_asal'         => $faker->randomElement($kota),
                'kecamatan_asal'    => $faker->randomElement(['Cibiru', 'Cileunyi', 'Cicaheum']),
                'kota_tujuan'       => 'Bandung',
                'kecamatan_tujuan'  => 'Regol',
                'alamat_lama'       => 'Jl. '.$faker->randomElement($jalan1).' No. '.$faker->randomDigitNotNull,
                'alamat_baru'       => 'Jl. '.$faker->randomElement($jalan2).' No. '.$faker->randomDigitNotNull,
                'jenis'             => 'datang',
                'agama'             =>  $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'kewarganegaraan'   =>  'WNI',
                'kelurahan'         => $faker->randomElement(['Ancol', 'Balonggede', 'Cigereleng', 'Ciateul', 'Ciseureuh', 'Pasirluyu', 'Pungkur']),
                'jenis_kelamin'     => 'Laki - Laki',
                'created_at'        => $date, 
                'updated_at'        => $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }
        for ($i=1; $i <= 300; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_registrasi'     => 'DTG'.$faker->unique()->numberBetween($min = 801, $max = 1200),
                'nik'               => $faker->unique()->numberBetween($min = 9050001, $max = 9060000),
                'no_kk'             => $faker->unique()->numberBetween($min = 32739030001, $max = 32739050000),
                'nama'              => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'kota_asal'         => $faker->randomElement($kota),
                'kecamatan_asal'    => $faker->randomElement(['Cibiru', 'Cileunyi', 'Cicaheum']),
                'kota_tujuan'       => 'Bandung',
                'kecamatan_tujuan'  => 'Regol',
                'alamat_lama'       => 'Jl. '.$faker->randomElement($jalan1).' No. '.$faker->randomDigitNotNull,
                'alamat_baru'       => 'Jl. '.$faker->randomElement($jalan2).' No. '.$faker->randomDigitNotNull,
                'jenis'             => 'datang',
                'agama'             =>  $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'kewarganegaraan'   =>  'WNI',
                'kelurahan'         => $faker->randomElement(['Ancol', 'Balonggede', 'Cigereleng', 'Ciateul', 'Ciseureuh', 'Pasirluyu', 'Pungkur']),
                'jenis_kelamin'     => 'Perempuan',
                'created_at'        => $date, 
                'updated_at'        => $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }

        for ($i=1; $i <= 500; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_registrasi'     => 'PRG'.$faker->unique()->numberBetween($min = 1201, $max = 2000),
                'nik'               => $faker->unique()->numberBetween($min = 9060001, $max = 9070000),
                'no_kk'             => $faker->unique()->numberBetween($min = 32739050001, $max = 32739070000),
                'nama'              => $faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'kota_asal'         => 'Bandung',
                'kecamatan_asal'    => 'Regol',
                'kota_tujuan'       => $faker->randomElement($kota),
                'kecamatan_tujuan'  => $faker->randomElement(['Lebakbulus', 'Fatmawati', 'Kuta']),
                'alamat_lama'       => 'Jl. '.$faker->randomElement($jalan1).' No. '.$faker->randomDigitNotNull,
                'alamat_baru'       => 'Jl. '.$faker->randomElement($jalan2).' No. '.$faker->randomDigitNotNull,
                'jenis'             => 'pergi',
                'agama'             =>  $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'kewarganegaraan'   =>  'WNA',
                'kelurahan'         => $faker->randomElement(['Ancol', 'Balonggede', 'Cigereleng', 'Ciateul', 'Ciseureuh', 'Pasirluyu', 'Pungkur']),
                'jenis_kelamin'     => 'Laki - Laki',
                'created_at'        => $date, 
                'updated_at'        => $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }

        for ($i=1; $i <= 500; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_registrasi'     => 'PRG'.$faker->unique()->numberBetween($min = 2001, $max = 2600),
                'nik'               => $faker->unique()->numberBetween($min = 9070001, $max = 9080000),
                'no_kk'             => $faker->unique()->numberBetween($min = 32739070001, $max = 32739090001),
                'nama'              => $faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
                'kota_asal'         => 'Bandung',
                'kecamatan_asal'    => 'Regol',
                'kota_tujuan'       => $faker->randomElement($kota),
                'kecamatan_tujuan'  => $faker->randomElement(['Lebakbulus', 'Fatmawati', 'Kuta']),
                'alamat_lama'       => 'Jl. '.$faker->randomElement($jalan1).' No. '.$faker->randomDigitNotNull,
                'alamat_baru'       => 'Jl. '.$faker->randomElement($jalan2).' No. '.$faker->randomDigitNotNull,
                'jenis'             => 'pergi',
                'agama'             =>  $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'kewarganegaraan'   =>  'WNA',
                'kelurahan'         => $faker->randomElement(['Ancol', 'Balonggede', 'Cigereleng', 'Ciateul', 'Ciseureuh', 'Pasirluyu', 'Pungkur']),
                'jenis_kelamin'     => 'Perempuan',
                'created_at'        => $date, 
                'updated_at'        => $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }

        for ($i=1; $i <= 500; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_registrasi'     => 'PRG'.$faker->unique()->numberBetween($min = 2601, $max = 3500),
                'nik'               => $faker->unique()->numberBetween($min = 9068001, $max = 9090000),
                'no_kk'             => $faker->unique()->numberBetween($min = 32739090002, $max = 32739120000),
                'nama'              => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'kota_asal'         => 'Bandung',
                'kecamatan_asal'    => 'Regol',
                'kota_tujuan'       => $faker->randomElement($kota),
                'kecamatan_tujuan'  => $faker->randomElement(['Lebakbulus', 'Fatmawati', 'Kuta']),
                'alamat_lama'       => 'Jl. '.$faker->randomElement($jalan1).' No. '.$faker->randomDigitNotNull,
                'alamat_baru'       => 'Jl. '.$faker->randomElement($jalan2).' No. '.$faker->randomDigitNotNull,
                'jenis'             => 'pergi',
                'agama'             =>  $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'kewarganegaraan'   =>  'WNI',
                'kelurahan'         => $faker->randomElement(['Ancol', 'Balonggede', 'Cigereleng', 'Ciateul', 'Ciseureuh', 'Pasirluyu', 'Pungkur']),
                'jenis_kelamin'     => 'Laki - Laki',
                'created_at'        => $date, 
                'updated_at'        => $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }

        for ($i=1; $i <= 500; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_registrasi'     => 'PRG'.$faker->unique()->numberBetween($min = 3501, $max = 4500),
                'nik'               => $faker->unique()->numberBetween($min = 9090001, $max = 9092000),
                'no_kk'             => $faker->unique()->numberBetween($min = 32739120001, $max = 32739130001),
                'nama'              => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'kota_asal'         => 'Bandung',
                'kecamatan_asal'    => 'Regol',
                'kota_tujuan'       => $faker->randomElement($kota),
                'kecamatan_tujuan'  => $faker->randomElement(['Lebakbulus', 'Fatmawati', 'Kuta']),
                'alamat_lama'       => 'Jl. '.$faker->randomElement($jalan1).' No. '.$faker->randomDigitNotNull,
                'alamat_baru'       => 'Jl. '.$faker->randomElement($jalan2).' No. '.$faker->randomDigitNotNull,
                'jenis'             => 'pergi',
                'agama'             =>  $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'kewarganegaraan'   =>  'WNI',
                'kelurahan'         => $faker->randomElement(['Ancol', 'Balonggede', 'Cigereleng', 'Ciateul', 'Ciseureuh', 'Pasirluyu', 'Pungkur']),
                'jenis_kelamin'     => 'Perempuan',
                'created_at'        => $date, 
                'updated_at'        => $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }
        DB::table('pindahs')->insert($data);
    }
}
